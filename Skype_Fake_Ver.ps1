[CmdletBinding()]
Param (
    [switch]$Recompress
)

$ErrorActionPreference = 'Stop'
$IsVerbose = ($false, $true)[$VerbosePreference -eq 'Continue']

trap {
    $_ | Out-String | Show-Text -ForegroundColor Red -BackgroundColor Black
    Pause -Message 'Press any key to exit...'
    exit 1
}

if (-not $PSScriptRoot) {
    $PSScriptRoot = Split-Path $script:MyInvocation.MyCommand.Path -Parent
}

$script:Cfg = @{
    InDir        = Join-Path $PSScriptRoot 'in'
    OutDir       = Join-Path $PSScriptRoot 'out'
    ApkTool      = Join-Path $PSScriptRoot 'tools\apktool\apktool_2.4.0.jar'
    SignTool     = Join-Path $PSScriptRoot 'tools\signapk\signapk.jar'
    ZipAlignTool = Join-Path $PSScriptRoot 'tools\zipalign\zipalign.exe'
    JavaPath     = $null
}

function Expand-APK {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias('FullName')]
        [string]$InFile,

        [ValidateScript( {
                Test-Path -LiteralPath $_ -PathType Container
            })]
        [string]$OutDir
    )

    Process {
        $Cmd = @(
            '-jar'
            '-Duser.language=en'
            '-Dfile.encoding=UTF8'
            $script:Cfg.ApkTool
            'd'
            $InFile
            '-f'
            '-o'
            $OutDir
            '-p'
            (Join-Path ([System.IO.FileInfo]$script:Cfg.ApkTool).Directory.FullName 'frameworks')
        )

        @(
            'Expanding APK:'
            @(
                $script:Cfg.JavaPath
                $Cmd
            ) -join ' '
        ) | Write-Verbose

        $OldEA = $ErrorActionPreference
        try {
            $ErrorActionPreference = 'Continue'
            & $script:Cfg.JavaPath $Cmd 2>&1 | Write-Verbose
        } finally {
            $ErrorActionPreference = $OldEA
        }

        if ($LASTEXITCODE) {
            throw "Failed to decompile APK: $InFile"
        }
    }
}

function Compress-APK {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [ValidateScript( {
                Test-Path -LiteralPath $_ -PathType Container
            })]
        [string]$InDir,

        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$OutFile
    )

    Process {
        $Cmd = @(
            '-jar'
            '-Duser.language=en'
            '-Dfile.encoding=UTF8'
            $script:Cfg.ApkTool
            'b'
            $InDir
            '-o'
            $OutFile
            '-p'
            (Join-Path ([System.IO.FileInfo]$script:Cfg.ApkTool).Directory.FullName 'frameworks')

        )

        @(
            'Compressing APK:'
            @(
                $script:Cfg.JavaPath
                $Cmd
            ) -join ' '
        ) | Write-Verbose

        $OldEA = $ErrorActionPreference
        try {
            $ErrorActionPreference = 'Continue'
            & $script:Cfg.JavaPath $Cmd 2>&1 | Write-Verbose
        } finally {
            $ErrorActionPreference = $OldEA
        }

        if ($LASTEXITCODE) {
            throw "Failed to compile APK: $InDir"
        }

        $OutFile
    }
}

function Sign-APK {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [ValidateScript( {
                Test-Path -LiteralPath $_ -PathType Leaf
            })]
        [string]$InFile,

        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$OutFile
    )

    Process {
        $Cmd = @(
            '-jar'
            $script:Cfg.SignTool
            (
                'testkey.x509.pem', 'testkey.pk8' | ForEach-Object {
                    Join-Path ([System.IO.FileInfo]$script:Cfg.SignTool).Directory.FullName $_
                }
            )
            $InFile
            $OutFile
        )

        @(
            'Signing APK:'
            @(
                $script:Cfg.JavaPath
                $Cmd
            ) -join ' '
        ) | Write-Verbose

        $OldEA = $ErrorActionPreference
        try {
            $ErrorActionPreference = 'Continue'
            & $script:Cfg.JavaPath $Cmd 2>&1 | Write-Verbose
        } finally {
            $ErrorActionPreference = $OldEA
        }

        if ($LASTEXITCODE) {
            throw "Failed to sign APK: $inFile"
        }

        $OutFile
    }
}

function Optimize-APK {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [ValidateScript( {
                Test-Path -LiteralPath $_ -PathType Leaf
            })]
        [string]$InFile,

        [Parameter(Mandatory = $true)]
        [ValidateNotNullOrEmpty()]
        [string]$OutFile,

        [switch]$Recompress
    )

    Process {
        $Cmd = @(
            '-f'
            $(if ($Recompress) {'-z'})
            '4'
            $InFile
            $OutFile
        )

        @(
            'Optimizing APK:'
            @(
                $script:Cfg.ZipAlignTool
                $Cmd
            ) -join ' '
        ) | Write-Verbose

        $OldEA = $ErrorActionPreference
        try {
            $ErrorActionPreference = 'Continue'
            & $script:Cfg.ZipAlignTool $Cmd 2>&1 | Write-Verbose
        } finally {
            $ErrorActionPreference = $OldEA
        }

        if ($LASTEXITCODE) {
            throw "Failed to ziplaign APK: $InFile"
        }

        $OutFile
    }
}

function Get-APKVersion {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [Alias('FullName')]
        [string]$InFile
    )

    Process {
        $Version = New-Object PSCustomObject -Property @{
            versionCode = $null
            versionName = $null
        }


        Get-Content -Path $InFile | ForEach-Object {

            if ($_ -match '^\s*versionCode:\s*(.+)') {
                $Version.versionCode = ($Matches[1]).Trim("'")
            }

            if ($_ -match '^\s*versionName:\s*(.+)') {
                $Version.versionName = ($Matches[1]).Trim("'")
            }
        }

        Write-Verbose 'Detected version:'
        $Version | Format-Table -AutoSize | Out-String | Write-Verbose

        if (-not $Version.versionCode -or -not $Version.versionName) {
            throw "Can't find APK version in: $_"
        }

        $Version
    }

}

function Show-VersionChoice {
    [CmdletBinding()]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true)]
        [ValidateNotNullOrEmpty()]
        [version]$VersionName
    )

    Process {
        $SuggestedVersion = New-Object -TypeName version -ArgumentList @(
            ($VersionName.Major + 1)
            $VersionName.Minor
            $VersionName.Build
            $VersionName.Revision
        )

        do {
            Show-Text "*Set* Skype version [Enter: $SuggestedVersion]: " -ForegroundColor Yellow -NoNewline
            $NewVersion = (Read-Host)
        } while ($(
            if (-not $(try{[version]$NewVersion}catch{$false})) {
                if ([string]::Empty -eq ($NewVersion -replace '\s+')) {
                    $NewVersion = $SuggestedVersion
                    $false
                } else {
                    Write-Host "Invalid version: $NewVersion!"
                    $true
                }
            } else { $false }
        ))

        $NewVersion
    }
}

<#
.Synopsis
    Show text on screen, optionally center it and specify color.
    Centering works only in PS console, not ISE.
#>
function Show-Text
{
    [CmdletBinding()]
    Param
    (
        [Parameter(ValueFromPipeline = $true)]
        [string]$Text,

        [Parameter(ValueFromPipelineByPropertyName = $true)]
        [ValidateScript({
            [Enum]::GetValues([ConsoleColor]) -contains $_
        })]
        [string]$ForegroundColor,

        [Parameter(ValueFromPipelineByPropertyName = $true)]
        [ValidateScript({
            [Enum]::GetValues([ConsoleColor]) -contains $_
        })]
        [string]$BackgroundColor,

        [Parameter(ValueFromPipelineByPropertyName = $true)]
        [switch]$Center,

        [Parameter(ValueFromPipelineByPropertyName = $true)]
        [switch]$NoNewline,

        [Parameter(ValueFromPipelineByPropertyName = $true)]
        [switch]$AsSeparator
    )

    Begin
    {
        # Get console width (PS console only)
        $ConsoleWidth = $host.UI.RawUI.WindowSize.Width
    }

    Process
    {
        # This will allow Write-Host to use default values for ForegroundColor\BackgroundColor
        $WriteHost = {
            '$_ | Write-Host'
            if($ForegroundColor){'-ForegroundColor $ForegroundColor'}
            if($BackgroundColor){'-BackgroundColor $BackgroundColor'}
            if($NoNewline){'-NoNewline'}
        }

        # Treat Text as Separator?
        if($AsSeparator)
        {
            # Create separator
            $ret = $Text * ($ConsoleWidth / $Text.Length)
        }
        else
        {
            # If Center switch specified and we're in PS console
            if($Center -and $ConsoleWidth)
            {
                # Pad text to center it
                $ret = $Text | ForEach-Object {
                        $_ -split [System.Environment]::NewLine | ForEach-Object {
                            $PadSize = [System.Math]::Floor($ConsoleWidth/2 - $_.Length/2)
                            $_.PadLeft($_.Length + $PadSize)
                        }
                    }
            }
            else
            {
                # Do nothing to text
                $ret = $Text
            }
        }

        # Display text
        $ret  | ForEach-Object -Process ([scriptblock]::Create($WriteHost.InvokeReturnAsIs() -join ' '))
    }
}

<#
.Synopsis
	PowerShell pause implementation

.Link
	https://adamstech.wordpress.com/2011/05/12/how-to-properly-pause-a-powershell-script/
#>
function Pause {

    Param
    (
        [string]$Message = 'Press any key to continue...',

        [Parameter(ValueFromRemainingArguments = $true)]
        $PassMe
    )

	If ($psISE) {
		# The "ReadKey" functionality is not supported in Windows PowerShell ISE.

		#$Shell = New-Object -ComObject 'WScript.Shell'
		#$Button = $Shell.Popup('Click OK to continue.', 0, 'Script Paused', 0)

		return
	}

    if ($PassMe) {
	    Show-Text -Text $Message -NoNewline $PassMe
    } else {
        Show-Text -Text $Message -NoNewline
    }

	$Ignore =
		16,  # Shift (left or right)
		17,  # Ctrl (left or right)
		18,  # Alt (left or right)
		20,  # Caps lock
		91,  # Windows key (left)
		92,  # Windows key (right)
		93,  # Menu key
		144, # Num lock
		145, # Scroll lock
		166, # Back
		167, # Forward
		168, # Refresh
		169, # Stop
		170, # Search
		171, # Favorites
		172, # Start/Home
		173, # Mute
		174, # Volume Down
		175, # Volume Up
		176, # Next Track
		177, # Previous Track
		178, # Stop Media
		179, # Play
		180, # Mail
		181, # Select Media
		182, # Application 1
		183  # Application 2

	While ($KeyInfo.VirtualKeyCode -Eq $Null -Or $Ignore -Contains $KeyInfo.VirtualKeyCode) {
		$KeyInfo = $Host.UI.RawUI.ReadKey('NoEcho, IncludeKeyDown')
	}

	Write-Host
}


$Banner = @{
    Skype = @'
   _____ __                  
  / ___// /____  ______  ___ 
  \__ \/ //_/ / / / __ \/ _ \
 ___/ / ,< / /_/ / /_/ /  __/
/____/_/|_|\__, / .___/\___/ 
          /____/_/           
'@
    Tool = @'
   ___     __                          _                           __  __            __
  / _/__ _/ /_____   _  _____ _______ (_)__  ___    __ _  ___  ___/ / / /____  ___  / /
 / _/ _ `/  '_/ -_) | |/ / -_) __(_-</ / _ \/ _ \  /  ' \/ _ \/ _  / / __/ _ \/ _ \/ / 
/_/ \_,_/_/\_\\__/  |___/\__/_/ /___/_/\___/_//_/ /_/_/_/\___/\_,_/  \__/\___/\___/_/  

'@
}

Show-Text -Text $Banner.Skype -ForegroundColor Cyan -Center
Show-Text -Text $Banner.Tool -ForegroundColor Magenta -Center
Show-Text -Text '_' -AsSeparator -ForegroundColor Green

try {
    $Java = Get-Command -Name java -CommandType Application -ErrorAction Stop
    
    Write-Verbose 'Available Java versions:'
    $Java | Select-Object -Property Path, Version | Format-List | Out-String | Write-Verbose

    $script:Cfg.JavaPath = @($Java)[0].Path

    Write-Verbose 'Script configuration:'
    $script:Cfg | Format-Table -AutoSize | Out-String | Write-Verbose
} catch {
    Show-Text 'Java not installed!' -ForegroundColor Red
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green
    Pause -Message 'Press any key to exit...'
    exit 1
}

Get-ChildItem $script:Cfg.InDir -Filter '*.apk' | ForEach-Object {
    $UnpackedDir = Join-Path $script:Cfg.OutDir $_.BaseName

    if (-not (Test-Path -LiteralPath $UnpackedDir -PathType Container)) {
        Write-Verbose "Creating directory: $_"
        New-Item -Path $UnpackedDir -ItemType Directory | Write-Verbose
    }

    $ApkToolYml = Join-Path $UnpackedDir 'apktool.yml'

    if (-not (Test-Path -LiteralPath $ApkToolYml -PathType Leaf)) {
        Show-Text 'Decompiling APK file: ' -ForegroundColor Cyan -NoNewline
        Show-Text $_.FullName -ForegroundColor Yellow
        Show-Text -Text '_' -AsSeparator -ForegroundColor Green

        Expand-APK -InFile $_.FullName -OutDir $UnpackedDir -Verbose:$IsVerbose
    }

    $CurrentVersion = Get-ChildItem -Path $ApkToolYml | Get-APKVersion -Verbose:$IsVerbose

    Show-Text -Text 'Detected Skype version: ' -ForegroundColor Magenta -NoNewline
    Show-Text -Text $CurrentVersion.versionName -ForegroundColor Yellow
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green

    $NewVersionName = $CurrentVersion | Show-VersionChoice
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green
    Show-Text 'NEW Skype version: ' -ForegroundColor Magenta -NoNewline
    Show-Text $NewVersionName -ForegroundColor Yellow
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green

    if ($NewVersionName -eq $CurrentVersion.versionName) {
        Show-Text 'Versions match, skipping replace' -ForegroundColor Cyan
        Show-Text -Text '_' -AsSeparator -ForegroundColor Green
    }
    else {
        Show-Text 'Replacing version: ' -ForegroundColor Cyan -NoNewline
        Show-Text $CurrentVersion.versionName -ForegroundColor Green -NoNewline
        Show-Text ' -> ' -ForegroundColor Red -NoNewline
        Show-Text $NewVersionName -ForegroundColor Yellow

        Get-ChildItem -Path $UnpackedDir -Recurse | Where-Object {-not $_.PsIsContainer} |
        ForEach-Object {
            switch ($_) {
                {
                    $_.Extension -match '^\.(smali|xml|yml)$'
                } {

                    $in = [System.IO.File]::ReadAllText($_.FullName)
                    $out = $in -replace @(
                        [regex]::Escape($CurrentVersion.versionName)
                        $NewVersionName
                    )

                    if ($in -cne $out) {
                        Write-Verbose $_.FullName
                        [System.IO.File]::WriteAllText($_.FullName, $out)
                    }
                }
            }
        }
    }
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green

    $OutFile = Join-Path $script:Cfg.OutDir ($_.BaseName + "-[$NewVersionName].apk")
    $OutFileUnsigned = "$OutFile.unsigned"
    $OutFileSigned = "$OutFile.signed"

    Show-Text 'Removing previous APK files: ' -ForegroundColor Cyan -NoNewline
    Show-Text $_ -ForegroundColor Yellow
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green

    $OutFile | Remove-Item -Force

    $ret = $UnpackedDir | ForEach-Object {
        Show-Text 'Compiling APK: ' -ForegroundColor Cyan -NoNewline
        Show-Text $_ -ForegroundColor Yellow
        Show-Text -Text '_' -AsSeparator -ForegroundColor Green

        $_ | Compress-APK -OutFile $OutFileUnsigned -Verbose:$IsVerbose
    } | ForEach-Object {
        Show-Text 'Signing APK  : ' -ForegroundColor Cyan -NoNewline
        Show-Text $_ -ForegroundColor Yellow
        Show-Text -Text '_' -AsSeparator -ForegroundColor Green

        $_ | Sign-APK -OutFile $OutFileSigned -Verbose:$IsVerbose
    } | ForEach-Object {
        Show-Text 'Aligning APK : ' -ForegroundColor Cyan -NoNewline
        Show-Text $_ -ForegroundColor Yellow
        Show-Text -Text '_' -AsSeparator -ForegroundColor Green

        $_ | Optimize-APK -OutFile $OutFile -Recompress:$Recompress -Verbose:$IsVerbose
    }

    $OutFileUnsigned, $OutFileSigned | ForEach-Object {
        if (Test-Path -LiteralPath $_ -PathType Leaf) {
            Show-Text 'Removing temporary APK file: ' -ForegroundColor Cyan -NoNewline
            Show-Text $_ -ForegroundColor Yellow

            Remove-Item -LiteralPath $_ -Force -Verbose:$IsVerbose
        }
    }
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green

    Show-Text 'Done: ' -ForegroundColor Magenta -NoNewline
    Show-Text $ret -ForegroundColor Yellow
    Show-Text -Text '_' -AsSeparator -ForegroundColor Green  
}

Pause -Message 'Press any key...'