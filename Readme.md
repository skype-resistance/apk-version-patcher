# Fake version in Skype 7.x APK

## Usage

1. Put one ore more Skype 7.x APKs in `.\in` folder
2. Double click `Skype_Fake_Ver.cmd`
3. Press Enter to automatically increase major version, or enter custom version.
4. Wait for script to patch Skype files and create new APK.
5. Get new APK from `.\out` folder and install it on your device.

## Dependencies

Script needs Java installed to run APK tools.